# A bash project template

## 📘 Use this template

To use this template `git clone` it in your desired repository:
```
$ git clone https://gitlab.com/constellation-paas/bash-project-template.git my-super-duper-project
```

Then, go in the cloned project and delete the `.git` folder:
```
$ cd my-super-duper-project
$ rm -rf .git
```

## 💯 Testing

You can run the test with:
```
$ make test
```

## 📓 Resources

You can find the docs for [bats(1)][bats] (our test framework) here, in the
[ztombol/bats-docs repository][bats-doc]

[bats]: https://github.com/bats-core/bats-core/tree/v1.2.0
[bats-doc]: https://github.com/ztombol/bats-docs

## License

[MIT](LICENSE)
