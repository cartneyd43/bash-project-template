#!/usr/bin/env bats

load '../vendor/test-helper/bats-support/load'

bin=${BIN_TO_TEST:-"./my-script"}

## General options tests

@test "--help should return the usage" {
    run $bin --help

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "Usage"
}

@test "-h should return the usage" {
    run $bin -h

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "Usage"
}

@test "-V should return the version" {
    run $bin -V

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "v"
}
@test "--version should return version" {
    run $bin --version

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "v"
}
@test "A fake arg should be rejected with an error and print the usage" {
    run $bin --lol-this-is-fake

    [ "$status" -eq 1 ]
    echo "$output" | grep -q "Error"
    echo "$output" | grep -q "Usage"
}

